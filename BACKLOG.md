# Manage attachments

Pages and comments should point to attachments, using GitHub-markdown style relative links.

# Manage revisions

Output a humongous shell script that `cp(1)`s and `git commit`s things in the proper order, at the proper place (path and filename), at the proper (back-dated) time, with the proper author.

# Manage comments

Comments should live into a mytitle-comments/ subdirectory. They should be handled along with revisions (💡 Natalie says that moving a Confluence page destroys its comments)

# Page 72517250 is entirely orphan. What does that mean? Ask Michel

- Visible (not really) at https://confluence.epfl.ch:8443/pages/viewpage.action?pageId=72517250
- Is in draft state
- Nothing points to its ID (i.e. cannot be navigated to)
- Has no title, no body, and the author doesn't have a matching `<object class="ConfluenceUserImpl" />`

Is this normal? Is this a censorship artifact (meaning, a page that the exporter refused to export)?
