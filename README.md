# confluence2x

## Developer Instructions

```
yarn
rm -rf out; mkdir out; yarn convert /path/to/your/elements.xml out/
```

## Links

 - [confluence storage format](https://confluence.atlassian.com/doc/confluence-storage-format-790796544.html)
 - [front-matter](https://jekyllrb.com/docs/front-matter/)
 - [Pandoc's metadata variables](https://pandoc.org/MANUAL.html#metadata-variables)
 - Other tools:
    - https://github.com/highsource/confluence-to-markdown-converter
    - https://github.com/oberlies/confluence-to-markdown-converter
    - [grugnog's PHP](https://gist.github.com/grugnog/2e6ee420707a8196951956cf4dbf6e91)
    - https://github.com/gkpln3/ConfluenceToWikiJS
    - https://github.com/hallowelt/migrate-confluence
