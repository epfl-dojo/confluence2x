ENTITIES_XML = entities.xml

SAXON := $(shell which saxonb-xslt saxon 2>/dev/null |grep -v 'not found' | head -1)

.PHONY: all
all: work/markdown

work/xml: $(ENTITIES_XML)
	@mkdir -p $@ || true
	yarn convert $< $@
	touch $@

work/markdown: work/xml
	@mkdir -p $@ || true
	$(SAXON) -xsl:node/c2md.xsl -s:$< -o:$@
	touch $@

.PHONY: clean
clean:
	rm -rf work
