import fs from "fs"
import { program as lecommander } from "commander"
import { AcProcessor } from "./ac-processor"

lecommander.version("0.1.0");
lecommander
   .name("convert.ts")
   .argument("<inputxml>")
   .argument("<outputdir>")
   .option("--no-bodies", "Skip generating body XML files")
   .option("--no-meta", "Skip generating metadata.xml")
   .action(function(inputxml, outputdir, options) {
     const processor = new AcProcessor(fs.readFileSync(inputxml).toString());
     if (options.bodies) {
       processor.splitDocuments(outputdir);
     }
     if (options.meta) {
       processor.outputMetadata(`${outputdir}/metadata.xml`);
     }
   })
   .parse();
