const fs = require('fs')


// console.log(typeof entities['hibernate-generic']['object'])

class Confluence {
  constructor(concluence_xml_export_path) {
    const entitiesdata = fs.readFileSync(concluence_xml_export_path)
    this.AllEntities = JSON.parse(entitiesdata)
    this.data = this.AllEntities['hibernate-generic']['object']
  }

  propFinder = (arr, key, val) => {
    return arr.find(o => o[key] === val)
  }

  getPages = () => {
    this.pages = this.data.filter(el => {
      return el['@class'] === 'Page'
    })
    return this.pages
  }

  getPageById = (pageID) => {
    return this.pages.filter(el => {
      return el.id['#text'] == pageID
    })[0]
  }

  getPageTitle = (page) => {
    if (page) return this.propFinder(page['property'], '@name', 'title')['#text'] || null
  }
  getPageTitleById = (pageID) => {
    let page = this.getPageById(pageID)
    return this.getPageTitle(page) || null
  }

  getPageParentID = (page) => {
    if (page) return this.propFinder(page['property'], '@name', 'parent')?.id['#text'] || null
  }
  getPageParentIDById = (pageID) => {
    let page = this.getPageById(pageID)
    return this.getPageParentID(page)  || null
  }

}

const c = new Confluence('../entities.json')

//console.log(c.getPages())

let exit = 0
for (const element of c.getPages()) {
  exit++
  console.debug("\n\n===================================================")
  console.debug(c.getPageTitle(element))
  console.debug("Parent:", c.getPageParentID(element), "("+c.getPageTitleById(c.getPageParentID(element))+")")

  if (exit > 200) process.exit()
}
