
/**
 * The “model” for the entire Confluence data set.
 *
 * Responsibilities include creating and going through lookup tables
 * to e.g. associate a Page or Comment to its BodyContent, look up
 * users etc.
 */

import { Document, Element } from "libxmljs"
import { xmlObjId, xmlObjClass, xmlGetText } from "./xml-ac"

export class AcModel {
  public eachPage: (f: (page: Element, body : Element | null) => void) => void;
  public eachComment: (f: (comment: Element, body : Element | null) => void) => void;
  public getUser: (id : string) => Element | undefined;

  constructor (xmlDoc : Document) {
    const objects = {
      BodyContent: {} as { [ id : string ] : Element },
      Page: {} as { [ id : string ] : Element },
      Comment: {} as { [ id : string ] : Element },
      ConfluenceUserImpl: {} as { [ id : string ] : Element }
    };
    const bodyIDs : { [ id : string ] : string } = {};

    for (let obj of xmlDoc.find("//object")) {
      const objClass = xmlObjClass(obj);
      if (! (objClass === "Page" ||
            objClass === "Comment" ||
            objClass === "BodyContent" ||
            objClass === "ConfluenceUserImpl")) continue;

      const id = xmlObjId(obj);
      if (! id) {
        console.error(`Found a ${objClass} without an ID!`);
        continue;
      }

      objects[objClass][id] = obj;
      if (objClass !== "BodyContent") {
        const bodyId = xmlGetText(obj, 'collection[@name="bodyContents"]/element/id');
        if (bodyId) {
          bodyIDs[id] = bodyId;
        }
      }
    }

    return {
      eachPage (f) {
        for (let k in objects.Page) {
          f(objects.Page[k], bodyIDs[k] ? objects.BodyContent[bodyIDs[k]] : null);
        }
      },
      eachComment (f) {
        for (let k in objects.Comment) {
          f(objects.Comment[k], bodyIDs[k] ? objects.BodyContent[bodyIDs[k]] : null);
        }
      },
      getUser (id) {
        return objects.ConfluenceUserImpl[id]
      }
    };
  }
}
