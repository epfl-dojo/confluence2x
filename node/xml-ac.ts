/**
 * (Man)handle XML from Confluence.
 */
import { Element } from "libxmljs"

export function xmlGetText (obj : Element, xpath : string) : string | null {
  const got = obj.get(xpath);
  if (got) {
    return got.text();
  } else {
    return null;
  }
}

export function xmlObjId (obj : Element) {
  return xmlGetText(obj, 'id');
}

export function xmlObjClass (obj : Element) {
  return obj.attr('class')?.value();
}
