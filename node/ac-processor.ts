import libxslt, { libxmljs } from "libxslt"
import { Element } from "libxmljs"
import fs from "fs"
import { AcModel } from "./ac-model"
import { xmlObjId, xmlObjClass, xmlGetText } from "./xml-ac"
import * as entities from "html-entities"

export class AcProcessor {
  /**
   * Split out all `Page`s and all `Comment`s of `xmlDoc` into `outputdir`.
   *
   * Each `<object>` whose `@class` is either `Comment` or `Page` in
   * `xmlDoc`, is enumerated along with its `BodyContent`, and enough
   * data and metadata is aggregated into XML so as to transform later
   * into Markdown using an XSL stylesheet. Then, the resulting XML is
   * written into `${outputdir}/${class}${id}`, where `class` is
   * either `Page` or `Comment`, and `id` is the content of the `<id>`
   * child element.
   */
  public splitDocuments : (outputdir : string) => void;
  /**
   * Output a lightweight version of `xmlDoc` into `outputPath`.
   *
   * All the `<object class="BodyContent">`s are removed (on account
   * of having been processed by @link splitDocuments, or at least
   * those that are reachable from a Page or Comment). The rest of
   * `xmlDoc` is written into `outputPath` as-is.
   */
  public outputMetadata : (outputPath : string) => void;

  constructor (xmlString : string) {
    const xmlDoc = libxmljs.parseXml(xmlString);
    const model = new AcModel(xmlDoc);

    return {
      splitDocuments (outputdir : string) {
        model.eachPage((page, body) => {
          const id = xmlObjId(page);
          fs.writeFileSync(`${outputdir}/Page${id}`, xmlBody(model, page, body));
        })
        model.eachComment((comment, body) => {
          const id = xmlObjId(comment);
          fs.writeFileSync(`${outputdir}/Comment${id}`, xmlBody(model, comment, body));
        })
      },
      outputMetadata (outputPath: string) {
        fs.writeFileSync(outputPath,
          libxslt
            .parse(xslEverythingBut("object[@class='BodyContent']"))
            .apply(xmlDoc)
            .toString());
      }
    };
  }
}


function xmlBody (model : AcModel, parent : Element, body: Element | null) {
  const id = xmlObjId(parent),
  cls = xmlObjClass(parent),
  frontMatter = xmlFrontMatter(model, parent),
  title = xmlGetText(parent, 'property[@name="title"]') || "<no title>";

  let bodyText : string = "", moreAttributes: string = "";
  if (body === null) {
    bodyText = `This ${cls} has no body.`;
  } else {
    const bodyType = xmlGetText(body, 'property[@name="bodyType"]'),
          bodyLiteral = xmlGetText(body, 'property[@name="body"]');

    bodyText = (bodyType === '2') ?
      fixConfluenceFauxXhtml(bodyLiteral ?? "") :
      quoteXhtml(bodyLiteral);
    moreAttributes = `bodyType="${bodyType}"`;
  }

    return `
<ac:confluence xmlns:ac="http://www.atlassian.com/schema/confluence/4/ac/"
                      xmlns:ri="http://www.atlassian.com/schema/confluence/4/ri/"
                      xmlns="http://www.atlassian.com/schema/confluence/4/"
                      id="${id}" objectClass="${cls}" ${moreAttributes}>
  ${frontMatter}
  <pagetitle>${entities.encode(title)}</pagetitle>
  ${bodyText}
</ac:confluence>`;
}


function xmlFrontMatter (model : AcModel, parent : Element) {
  const title = xmlGetText(parent, 'property[@name="title"]') || "<no title>";
  const version = xmlGetText(parent, 'property[@name="version"]') || "<no version>";

  const creationDate = xmlGetText(parent, 'property[@name="creationDate"]') || "unknown";
  const lastModificationDate = xmlGetText(parent, 'property[@name="lastModificationDate"]') || "unknown";

  const creatorID = xmlGetText(parent, 'property[@name="creator"]/id') || "unknown";
  const creatorXML = model.getUser(creatorID);
  const creator = creatorXML ? xmlGetText(creatorXML, 'property[@name="name"]') : "unknown";

  const lastModifierID = xmlGetText(parent, 'property[@name="lastModifier"]/id') || "unknown";
  const lastModifierXML = model.getUser(lastModifierID);
  const lastModifier = lastModifierXML ? xmlGetText(lastModifierXML, 'property[@name="name"]') : "unknown";

  return `
<frontmatter>
  <title>${entities.encode(title)}</title>
  <version>${version}</version>
  <creationdate>${creationDate}</creationdate>
  <lastmodificationdate>${lastModificationDate}</lastmodificationdate>
  <creator>${creator}</creator>
  <lastauthor>${lastModifier}</lastauthor>
  <labels>
    <label>(WIP) aaa label</label>
    <label>(WIP) bbb label</label>
    <label>(WIP) ccc label</label>
  </labels>
</frontmatter>`;
}

function xslEverythingBut (xpath : string) {
  return `
<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

 <xsl:output omit-xml-declaration="yes"/>

    <xsl:template match="node()|@*">
      <xsl:copy>
         <xsl:apply-templates select="node()|@*"/>
      </xsl:copy>
    </xsl:template>

    <xsl:template match="${xpath}"/>
</xsl:stylesheet>
`;
}


function quoteXhtml (bodyLiteral : string | null) : string {
  if (bodyLiteral === null) return '';
  return entities.encode(bodyLiteral);
}

function fixConfluenceFauxXhtml (xhtml : string) : string {
  // Fix embedded faux CDATA:
  xhtml = replaceAll("](?:] | ])>", "]]>", xhtml);

  // entities.decode is overzealous with HTML entities inside URLs and
  // at the top level. Escape them using a heuristics:
  xhtml = xhtml.replace(/[&](amp|gt|lt|quot);/g, "&ESCAPED$1;");
  xhtml = entities.decode(xhtml);
  return replaceAll("&ESCAPED", "&", xhtml);
}

function replaceAll (replaceFrom : string | RegExp, replaceTo : string, str : string) : string {
  if (typeof(replaceFrom) === "string") {
    return str.replace(new RegExp(replaceFrom, 'g'), replaceTo);
  } else {
    return str.replace(replaceFrom, replaceTo);
  }
}
